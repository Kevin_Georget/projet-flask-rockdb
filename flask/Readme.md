# Rock DB

## But

Cette application prend les albums décrits dans le fichier
albums.yml et les images correspondantes dans static/images
pour les afficher en mode responsive avec Bootstrap.

## Pour installer

- Créer un virtualenv: virtualenv -p python3 venv
- L'activer: source venv/bin/activate
- Installer les dépendances du projet: 
   pip install -r requirements.txt

# Membre du groupe
Kevin Georget
Alfred Regneau
Erwan Carneiro
