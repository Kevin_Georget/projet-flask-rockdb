import click
from .app import app, db
from .models import *


@app.cli.command()
def syncdb():
    '''
    Création de toutes les tables de la BD
    '''
    db.create_all()


@app.cli.command()
@click.argument('filename')
@click.argument('images_folder')
def loaddb(filename, images_folder="images/"):
    '''
    Create all tables and populate them with data in filename
    '''
    db.drop_all()
    db.create_all()
    import yaml
    albums = yaml.load(open(filename), Loader=yaml.FullLoader)

    # Création des artistes
    artists = create_artist(albums)

    # Création des albums
    create_albums(albums, artists, images_folder)

    # Création des genres
    genres = create_genre(albums)

    # Création des relations genre-album
    belongs = create_belong(albums, genres)

@app.cli.command()
@click.argument('nom')
@click.argument('mdp')
def add_user(nom,mdp):
    """
    Permet de créer un utilisateur dans la base de donnée
    """
    utilisateur = Utilisateur(nom=nom)
    utilisateur.set_password(mdp)
    db.session.add(utilisateur)
    db.session.commit()
    return utilisateur

def create_artist(albums):
    artists = {}
    for a in albums:
        auths = [a["by"], a["parent"]]
        for artist in auths:
            if artist not in artists:
                obj = Artist(name=artist)
                # On ajoute l'objet obj à la Base :
                db.session.add(obj)
                artists[artist] = obj
    # On dit à la DB d'intégrer toutes les nouvelles données
    # ce qui permet aux auteurs d'avoir un id
    db.session.commit()
    return artists


def create_albums(albums, artists, images_folder):
    for a in albums:
        # on récupère l'objet Artiste correspondant
        by = artists[a["by"]]
        parent = artists[a["parent"]]
        img = a["img"]
        if img == None:
            img = 'indisponible.png'
        else:
            # 'fixtures/images/'
            img = images_folder + img
        obj = Album(id=a["entryId"],
                    title=a["title"],
                    img=img,
                    release_year=a["releaseYear"],
                    artist_id=by.id,
                    parent_id=parent.id
                    )
        # On ajoute l'objet album à la Base :
        db.session.add(obj)
    # on commite
    db.session.commit()

def create_genre(albums):
    genres = {}
    for a in albums:
        for genre in a["genre"]:
            if genre not in genres:
                obj = Genre(name=genre)
                db.session.add(obj)
                genres[genre] = obj
    db.session.commit()
    return genres

def create_belong(albums, genres):
    belongs = {}
    for a in albums:
        for genre in a["genre"]:
            g = genres[genre]
            obj = Belong(album_id=a["entryId"],
                         genre_id=g.id
                         )
            db.session.add(obj)
    db.session.commit()
