from flask_sqlalchemy import SQLAlchemy
import os.path
from flask_bootstrap import Bootstrap
from flask import Flask
app = Flask(__name__)

Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True


def mkpath(p):
    """
     renvoie repertoire courant
    """
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))


app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = (
    'sqlite:///'+ mkpath('../rockdb.db'))

app.config['SECRET_KEY'] = "930eff8d-813a-421b-b3df-3e457419c412"

db = SQLAlchemy(app)

from flask_login import LoginManager
login_manager = LoginManager(app)
